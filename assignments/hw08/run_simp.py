import csv
import os
import math
import collections
import sys
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

###################################################
# Global Variables
###################################################
HIT_TIME = 1
SIZES = [2**x for x in range(5,13)]
blockSizes = [2**x for x in range(0,6)]
associativity = [x for x in range(1,9)]
#SIZES = [2**x for x in range(5,7)]
#blockSizes = [2**x for x in range(0,2)]
#associativity = [x for x in range(1,3)]


# These three variables hold the iCache miss rates, the dCache miss rates, and the AMAT for each relevant plot
iCacheMissRates = collections.defaultdict(dict)
dCacheMissRates = collections.defaultdict(dict)
AMAT = collections.defaultdict(dict)
###################################################
# Functions
###################################################
# AMAT_Calculator() uses the Miss-Rate values stored in dCacheInput to calculate the average memory access time
# according to the formula given in the homework
def AMAT_Calculator(dCacheInput):
	for size in SIZES:
		for blockSize in blockSizes:
			miss_penalty = 2 + int(blockSize)
			miss_rate = float(dCacheInput[size][blockSize])
			miss_cycles = miss_penalty * miss_rate
			totalTime = miss_cycles + HIT_TIME
			AMAT[size][blockSize] = (totalTime)

# getY() is simply a helper function that extracts the requested values from their dictionaries and places them into 
# a numpy list so they can be used to plot
def getY(cacheType, currSize):
	values = []
	if cacheType != iCacheMissRates:
		for blockSize in blockSizes:
			print currSize, type(currSize)
			print blockSize, type(blockSize)
			missRate = cacheType[currSize][blockSize]
			print 'MissRate',missRate
			values.append(missRate)
	else:
		for assoc in associativity:
			values.append(cacheType[currSize][assoc])

	values = np.array(values)
	return values

# getCmd() is given either "wordsPerBlock", or "ways", and uses whichever of the two it is passed, along with
# size, and whatever the given constant is (associativity=1 or blockSize=4) 
# to calulate the three arguments to isimp, and return the full terminal command string
def getCmd(size, ways = None, wordsPerBlock = None):
	#Returns the values, base 2, of a cache's associativity, set-offset,and block-offset
	dCacheInput = True
	cacheFlag = '-dcache'
	if wordsPerBlock is None:
		dCacheInput = False
		cacheFlag = '-icache'

	if dCacheInput:
		sets = size / wordsPerBlock
		bytes_per_block = wordsPerBlock*4
		log2_sets = int(math.log(sets, 2))
		log2_bytes = int(math.log(bytes_per_block, 2))	
		waysOut = 1
	else:
		blocks_per_set = 4*ways;
		sets = size / blocks_per_set	
		log2_sets = int(math.log(sets, 2))
		log2_bytes = 4
		waysOut = ways

	cmd = ('isimp ' +
		cacheFlag + ' ' +
		str(waysOut) + ' ' +
		str(log2_sets) + ' ' +
		str(log2_bytes) +
		' a.out')	

	print cmd
	return cmd

def getCSVInfo(path='stats.csv', dCache = True):
	f = open(path)
	reader = csv.reader(f)
	my_dict = dict(reader)
	if dCache:
		result = my_dict['dCacheMissRate']
	else:
		result = my_dict['iCacheMissRate']

	return result;

# while it would be more effecient to nest certain parts of this code that apply to more than one of the 
# dictionaries, I thought it would be easier to understand like this
def setAxisOptions(figureName, cacheType):
	ax = figureName.add_subplot(111)
	if cacheType == AMAT:
		ax.set_ylim([1,2])
		ax.set_ylabel('AMAT')
		ax.set_xscale('log',basex=2)
		ax.set_xlim([1, 32])
		ax.set_xlabel('Block Size (words)')
		ax.set_title('AMAT vs. Block Size and Capacity (in words)')
	elif cacheType == dCacheMissRates:
		ax.set_ylim([0, .7])
		ax.set_ylabel('Miss Rate')
		ax.set_xscale('log',basex=2)
		ax.set_xlim([1, 32])
		ax.set_xlabel('Block Size (words)')
		ax.set_title('Miss Rate vs. Block Size and Capacity (in words)')
	else:
		ax.set_ylim([0, .3])
		ax.set_ylabel('Miss Rate')
		ax.set_xlim([1, 8])
		ax.set_xlabel('Associativity')
		ax.set_title('Miss Rate vs. Associativity and Capacity (in words)')

	return ax

# savePlot not only saves a plot's image, it actually 1. Creates a plot object, 2. Assigns the object the relevant Settings
# 3. Calculate's Y-values, 4. Plot's the elements, 5. stores the plot image in a local directory 
def savePlot(cacheType):
	fig = plt.figure()                              # 1.
	axis = setAxisOptions(fig, cacheType)           # 2.
	print str(cacheType)
	print 'TESTING SAVE PLOT'
	# if it is iCacheMissRaters
	if cacheType == AMAT:
		print 'CACHE_TYPE = AMAT'
		image_path_str = './matrix_output/AMAT_plot.png'
		csv_path_str = './matrix_output/AMAT.csv'
		xVals = np.array(blockSizes)	
	elif cacheType == dCacheMissRates:
		print 'CACHE_TYPE = dCache'
		image_path_str = './matrix_output/dCache_plot.png'
		csv_path_str = './matrix_output/dCache.csv'
		xVals = np.array(blockSizes)
	else:
		print 'CACHE_TYPE = iCache'
		xVals = np.array(associativity)
		image_path_str = './matrix_output/iCache_plot.png'
		csv_path_str = './matrix_output/iCache.csv'

	for size in SIZES:
		yVals = getY(cacheType, size)                # 3.
		axis.plot(xVals, yVals, '-o', label=size)    # 4.

	# if the plots info is not yet stored on a csv 
	if not os.path.isfile(csv_path_str):
		with open(csv_path_str, 'wb') as csvfile:
			csv_writer = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
			for size in SIZES:
				# if it is iCach, will loop associativity. Will loop Block size otherwise
				if cacheType == iCacheMissRates:
					print 'iCacheMissRates', iCacheMissRates
					for accos in associativity:
						tmpMiss =iCacheMissRates[size][accos]
						csv_writer.writerow([size, accos, tmpMiss])
				else:
					for blockSize in blockSizes:
						if cacheType == dCacheMissRates:
							print 'dCacheMissRates', dCacheMissRates
							tmpMiss = dCacheMissRates[size][blockSize]
							csv_writer.writerow([size, blockSize, tmpMiss ])
						else:
							tmpAMAT = AMAT[size][blockSize]
							print 'AMAT', cacheType
							csv_writer.writerow([size,blockSize,tmpAMAT])
	else:
		print 'TEST 1'

	# save the png
	axis.legend()                                   # 2.
	fig.savefig(image_path_str)                     # 5.



###################################################
# Main Execution
###################################################

if __name__ == '__main__':
	dCache_csv_str = './matrix_output/dCache.csv'	
	iCache_csv_str = './matrix_output/iCache.csv'
	AMAT_csv_str = './matrix_output/AMAT.csv'
	# All major steps in the code take place within a "for size in SIZES:" loop
	# each "size" in "SIZES" corresponds to a different line being plotted - a different total cache size
	for size in SIZES:

		# if the dCache MissRate results are not yet stored on the csv 
		if not os.path.isfile(dCache_csv_str):
			# for each possible blockSize, dCacheMissRates needs to calculate the miss rate
			for blockSize in blockSizes:
				# 'cmd' is the full string of the isimp command (e.g. isipm -dcache 1 4 2)
				cmd = getCmd(size, wordsPerBlock = blockSize)
				os.system(cmd)
				dCacheMissRates[size][blockSize] = getCSVInfo()
		else:
			with open(dCache_csv_str, 'rb') as dCacheCSV:
				dCacheReader = csv.reader(dCacheCSV, delimiter=' ',quotechar='|')
				for csvSize, csvBlockSize, csvMissRate in dCacheReader:
					dCacheMissRates[csvSize][csvBlockSize] = csvMissRate


		# if the hit-rate results are not yet stored on the csv 
		if not os.path.isfile(iCache_csv_str):
			# for each possible level of associativity, iCacheMissRates needs to calculate the MissRate
			for assocLevel in associativity:
				cmd = getCmd(size, ways = assocLevel)
				os.system(cmd)
				iCacheMissRates[size][assocLevel] = getCSVInfo(dCache = False)
		else:
			with open(iCache_csv_str, 'rb') as iCacheCSV:
				dCacheReader = csv.reader(iCacheCSV, delimiter=' ',quotechar='|')
				for csvSize, csvBlockSize, csvMissRate in dCacheReader:
					iCacheMissRates[csvSize][csvBlockSize] = csvMissRate



	# if the hit-rate results are not yet stored on the csv 
	if not os.path.isfile(AMAT_csv_str):
		AMAT_Calculator(dCacheMissRates)
	else:
		with open(AMAT_csv_str, 'rb') as AMATCSV:
 			AMATReader = csv.reader(AMATCSV, delimiter=' ',quotechar='|')
 			for csvSize, csvBlockSize, csvCycles in AMATReader:
 				iCacheMissRates[csvSize][csvBlockSize] = csvMissRate


	# Create and store plots
	savePlot(dCacheMissRates)
	savePlot(iCacheMissRates)
	savePlot(AMAT)
	os.system('rm stats.csv')
	
