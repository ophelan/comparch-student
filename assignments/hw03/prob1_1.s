# problem1_1.s    
    .set noreorder
    .data
    .text
    .globl main
    .ent main
main:
# int main()
# {
# int score = 84
        addi $s0, $0, 84        # s0 ("score") = 84  

# if (score >= 90 )
        slt $t0, $s0, 90        # if "score < 90 " $t0 = 1
        beq $t0, $0, else1      # if "score >= 90 goto(else1)"
        nop

# if (score >= 80 )
        slt $t0, $s0, 80        # if "score < 80 " $t0 = 1
        beq $t0, $0, else2      # if "score >= 80 goto(else2)"
        nop

# if (score >= 70)
        slt $t0, $s0, 70        # if "score < 70 " $t0 = 1
        beq $t0, $0, else3     # if "score >= 70 goto(else3)"
        nop

# grade = 4
else1:
        addi $s1, $0, 4        # $s1 ("grade") = 4
        j   exit
        nop

# grade = 3
else2:
        addi $s1, $0, 3        # $s1 ("grade") = 3
        j   exit
        nop

# grade = 2
else3:
        addi $s1, $0, 2        # $s1 ("grade") = 2
        j   exit
        nop

# grade = 0
        addi $s1, $0, 0        # $s1 ("grade") = 0

exit:
# PRINT_HEX_DEC(grade)
        ori $v0, $0, 20         # $v0 = 20
        add $a0, $0, $s1
        syscall
# EXIT
        ori $v0, $0, 10             # exit
        syscall
#}
    .end main

