# problem1_3.s
    .set noreorder
    .data
# int A[8]
array:    .space 32
    .text
    .globl main
    .ent main
# int main(){
main:
#int i
# A[0] = 0;
        la      $s0, array          # s0 (&array) = address of array
        addi    $t1, $0, 0          # t1 = 0
        sw      $t1, 0($s0)         # array[0] = t1
# A[1] = 1
        addi    $t1, $0, 1          # t1 = 1
        sw      $t1, 4($s0)         # array[1] = t1
# for ( i = 2.....)
        addi    $s1, $0, 2          # s1 (i) = 2

loop_begin:
# for (... i < 8 ...)
        slti    $t1, $s1, 8        # t1 = 1 if (i < 8)
        beq     $t1, $0, loop_end  # if i >= 8 go to loop_end
        nop
# A[i] = A[i-1] + A[i-2]
        add    $t1, $s1, $s1        # t1 = i*2
        add    $t1, $t1, $t1        # t1 = i*4
        add    $s2, $t1, $s0        # s2 = &array[i]
    
        addi    $t1, $s2, -4        # t1 = &array[ i - 1 ]
        lw      $t2, 0($t1)         # t2 = array[i-1]

        addi    $t1, $s2, -8        # t1 = &array[ i - 2 ]
        lw      $t3, 0($t1)         # t3 = array[i-2]

        add     $t2, $t2, $t3       # t2= array[i-1] + array[i-2]
        
        sw $t2, 0($s2)              # array[i] = array[i-1] + array[i-2]
# PRINT_HEX_DEC(A[i])
        ori     $v0, $0, 20              # print
        add     $a0, $0, $t2
        syscall
# i++
        addi    $s1, $s1, 1         # i = i + 1

        j loop_begin
        nop
loop_end:
# EXIT
        ori     $v0, $0, 10             # exit
        syscall
#}
    .end main

