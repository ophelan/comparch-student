# sum6.s
    .set noreorder
    .data

    .text

    .globl print
    .ent print
print:
# int print(x)
#{
        ori     $v0, $0, 20 
        syscall
        jr      $ra
        nop
    .end print

    .globl sum3
    .ent sum3
sum3:
# int sum3(int a, int b, int c)
        add     $v0, $a0, $a1   # v0 = a0 + a1
        add 	 $v0, $v0, $a2   # v0 = a0 + a1 + a2
        jr      $ra             # return
        nop
    .end sum3

    .globl polynomial
    .ent polynomial
polynomial:
		  addi $sp, $sp, -16
		  sw 	 $ra, 12($sp)
		  sw 	 $s0, 8($sp)
		  sw   $s1, 4($sp)
		  sw   $s2, 0($sp)
		  sll  $s0, $a0, $a1   # s0(x) = a << b
		  sll  $s1, $a2, $a3   # s1(y) = c << d
# z = sum3(x, y, e)
		  add  $a0, $0, $s0		#a0 = x
		  add  $a1, $0, $s1 	   #a1 = y
		  lw   $a2, 16($sp)		#a2 = e

		  jal 	sum3
		  nop

		  add  $s2, $0, $v0	  # s2(z) = sum(x, y, e)
# print(x)
		  add  $a0, $0, $s0	  # a0 = x
		  jal print
		  nop
# print(y)
		  add  $a0, $0, $s1	  # a0 = y
		  jal print 
		  nop 
# print(z)
		  add  $a0, $0, $s2    # a0 = z
		  jal print
		  nop

# return z
		  add $v0, $0, $s2
# restore values 
		  lw 	 $ra, 12($sp)
		  lw 	 $s0, 8($sp)
		  lw   $s1, 4($sp)
		  lw   $s2, 0($sp)		  
		  addi $sp, $sp, 16

		  jr 		$ra
		  nop

	 .end polynomial 

    .globl main
    .ent main
main:
# int main()
# {
        # PUSH ra, s0, s1 space for parameters e (16 bytes)
        add     $sp, $sp, -12
        sw      $ra, 8($sp)
        sw      $s0, 4($sp)
        sw		 $s1, 0($sp)
#		int a = 2
		  addi 	 $s0, $0, 2				# s0 (a) = 2
#     int f = polynomial(a, 3, 4, 5, 6);
        add     $a0, $0, $s0        # a0 = a
        addi    $a1, $0, 3          # a1 = 2
        addi    $a2, $0, 4          # a2 = 3
        addi    $a3, $0, 5          # a3 = 4
        addi    $t0, $0, 6          # t0 = 6
        sw      $t0, 0($sp)         # *(sp) = t0

        jal     polynomial          # v0 = call sum6
        nop

        add     $s1, $0, $v0		   # s1 (f)= call sum6
# 		print a
		  add 	 $a0, $0, $s0			# a0 = a
		  ori     $v0, $0, 20			# print(a)
        syscall
# 		print f
		  add 	 $a0, $0, $s1			# a0 = f
		  ori 	 $v0, $0, 20			# print(f)
		  syscall

#		return to function that called main
		  lw		 $s1, 0($sp)
		  lw 		 $s0, 4($sp)
		  lw 		 $ra, 8($sp)
		  addi	 $sp, $sp, 16

        jr      $ra
        nop
# }
    .end main
