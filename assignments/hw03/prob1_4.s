    .set noreorder
    .data

    .text
    .globl main
    .ent main
# int main(){
main:
# MALLOC (sizeof(record))
        ori     $v0, $0, 9          # v0 = 9, used for syscall MALLOC
        addi    $a0, $0, 8          # a0 = 8, 8 new bytes allocated
        syscall                     # v0 = address of allocated memory 
# record *r = (record *) MALLOC(sizeof(record))
        add     $s0, $0, $v0        # s0 = the address of allocated memory
# r->field0 = 100
        addi    $t1, $0, 100        # t1 = 100
        sw      $t1, 0($s0)         # record->field0 = t1
# r->field1 = -1
        addi    $t1, $0, -1         # t1 = 1
        sw      $t1, 4($s0)         # record->field1 = t1
# EXIT
        ori     $v0, $0, 10             # exit
        syscall
#}
    .end main

