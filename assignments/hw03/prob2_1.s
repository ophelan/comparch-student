# problem2_1.s
    .set noreorder
    .data
    .text

    .globl print
    .ent print
print:
# void print(int a)
# {
#     print a
        ori     $v0, $0, 20     # v0 = 20
        syscall                 # PRINT(v0)
        j       $ra             # return to $ra
        nop
# }
    .end print

    .globl main
    .ent main
main:
        add     $sp, $sp, -12        # Make space for 4 bytes on stack
        sw      $ra, 8($sp)         # $ra that existed when main was called is stored
        sw      $s0, 4($sp)
        sw      $s1, 0($sp)

        ori     $v0, $0, 9          # v0 = 9, used for syscall MALLOC
        addi    $a0, $0, 32         # a0 = 32, 32 new bytes wil be allocated
        syscall                     # v0 = address of allocated memory
        add     $s0, $0, $v0        # s1 = adress of 8-int arrary

        addi    $t1, $0, 0          # t1 = 0
        sw      $t1, 0($s0)         # array[0] = t1
        addi    $t1, $0, 1          # t1 = 1
        sw      $t1, 4($s0)         # array[1] = t1

        addi    $s1, $0, 2          # s1 = i = 2

loop_begin:
        slti    $t1, $s1, 8        # t1 = 1 if (i < 8)
        beq     $t1, $0, loop_end  # if i >= 8 go to loop_end
        nop

        add    $t1, $s1, $s1        # t1 = i*2
        add    $t1, $t1, $t1        # t1 = i*4
        add    $t2, $t1, $s0        # t2 = &array[i]
        
        addi    $t1, $t2, -4        # t1 = &array[ i - 1 ]
        lw      $t3, 0($t1)         # t3 = array[i-1]

        addi    $t1, $t1, -4        # t1 = &array[ i - 2 ]
        lw      $t4, 0($t1)         # t4 = array[i-2]

        add     $t3, $t3, $t4        #$t3 = array[i-1] + array[i-2]
        
        sw      $t3, 0($t2)         # array[i] = array[i-1] + array[i-2]

        add     $a0, $0, $t3        # a0 = array[i]     
        jal     print               # print (a0) = print(array[i])
        nop

        addi    $s1, $s1, 1         # i = i + 1
        j loop_begin                # jump to loop beginning 
        nop

loop_end:
        lw      $ra, 8($sp)         # restore the ra value
        lw      $s0, 4($sp)         # restore s0
        lw      $s1, 0($sp)
        add     $sp, $sp, 12         # restore the stack pointer
        jr      $ra                 # return to the function that called main
        nop
    .end main
