# problem1_2.s
    .set noreorder
    .data
# char A[] = {1, 25, 7, 9, -1}
array:  .word 1, 25, 7, 9, -1
    .text
    .globl main
    .ent main
#int main() {
main:
# max = 0
        addi    $s1, $0, 0          # s1 (max) = 0
# current = A[0]
        la      $t0, array          # t0 = address of array
        lw      $s0, 0($t0)         # s0 (current) = array[0]   

loop_begin:
# while ( current > 0 )
        slt     $t1, $0, $s0        # t1 = 1 if (current > 0)
        beq     $t1, $0, loop_end   # if current <= 0 go to loop_end
        nop

# if (current > max)
        slt     $t1, $s1, $s0           # t1 = 1 if max < current
        beq     $t1, $0, cond_end       # if max >= current go to cond_end
        nop
# max = current
        add     $s1, $0, $s0            # max = current
cond_end:
# i = i + 1
        addi    $t0, $t0, 4         # t0 = address of next item in array
# current = A[i]
        lw      $s0, 0($t0)        # current = array[i]

        j   loop_begin          # goto loop_cond
        nop
loop_end:
# PRINT_HEX_DEC(max)
        ori     $v0, $0, 20              # print
        add     $a0, $0, $s1
        syscall
# EXIT
        ori     $v0, $0, 10             # exit
        syscall
#}
    .end main

