    .set noreorder
    .data

    .text
    .globl main
    .ent main
#int main(){
main:
# elt *newelt = (elt *) MALLOC(sizeof (elt));
        ori     $v0, $0, 9          # v0 = 9, used for syscall MALLOC
        addi    $a0, $0, 8          # a0 = 8, 8 new bytes allocated
        syscall                     # v0 = address of allocated memory 
        add     $s1, $0, $v0        # s1 = *newelt = the address of allocated memory

# newelt->value = 1
        addi    $t1, $0, 1          # t1 = 1
        sw      $t1, 0($s1)         # s1 -> value = 1
# newelt->next = 0
        addi    $t1, $0, 0          # t1 = 0
        sw      $t1, 4($s1)         # s1 -> next = 0
# head = newelt
        add     $s0, $0, $s1        # s0 (head) = newelt

# newelt = (elt *) MALLOC(sizeof (elt));
        ori     $v0, $0, 9          # v0 = 9, used for syscall MALLOC
        syscall
        add     $s1, $0, $v0        # s1 = *newelt = address of allocated memory
# newelt->value = 2
        addi    $t1, $0, 2          # t1 = 2
        sw      $t1, 0($s1)         # newelt->value = 2
# newelt->next = head
        sw      $s0, 4($s1)         # newelt-> next = head
# head = newelt
        add     $s0, $0, $s1        # s0 (head) = newelt
# PRINT_HEX_DEC(head->value)
        ori     $v0, $0, 20         # v0 = 20, used for syscall PRINT
        lw      $t0, 0($s0)         # t0 = head -> value 
        add     $a0, $0, $t0        # a0 = head -> value    
        syscall                     # PRINT ( head -> value )
# PRINT_HEX_DEC(head->next->value)
        ori     $v0, $0, 20         # v0 = 20, used for sysvall PRINT
        lw      $t0, 4($s0)         # t0 = head -> next
        lw      $t1, 0($t0)         # t1 = head -> next -> value
        add     $a0, $0, $t1        # a0 = head -> next -> value
        syscall
# EXIT
        ori     $v0, $0, 10         # exit
        syscall
#}
    .end main

